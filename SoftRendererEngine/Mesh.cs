﻿using SharpDX;

namespace SoftRendererEngine
{
    public struct Face
    {
        public int A;
        public int B;
        public int C;
    }

    public class Mesh
    {
        public string Name;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3[] Vertices;
        public Face[] Faces;

        public Mesh(string name, int vertices, int facesCount)
        {
            Name = name;
            Vertices = new Vector3[vertices];
            Faces  = new Face[facesCount];
        }
    }
}