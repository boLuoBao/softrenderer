﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using SharpDX;

namespace SoftRendererEngine
{
    internal class Device
    {
        private readonly int renderWidth;
        private readonly int renderHeight;
        private readonly byte[] backBuffer;
        private readonly WriteableBitmap bmp;
        private readonly float[] depthBuffer;
        private readonly object[] lockBuffer;

        public Device(WriteableBitmap bmp)
        {
            this.bmp = bmp;
            renderWidth = bmp.PixelWidth;
            renderHeight = bmp.PixelHeight;

            backBuffer = new byte[renderHeight * renderWidth * 4];
            depthBuffer = new float[renderWidth * renderHeight];
            lockBuffer = new object[renderWidth * renderHeight];

            for (var i = lockBuffer.Length - 1; i >= 0; i--)
                lockBuffer[i] = new object();
        }

        public void Clear(byte r, byte g, byte b, byte a)
        {
            for (var i = backBuffer.Length - 1; i >= 0; i -= 4)
            {
                backBuffer[i] = a;
                backBuffer[i - 1] = r;
                backBuffer[i - 2] = g;
                backBuffer[i - 3] = b;
            }

            for (var i = depthBuffer.Length - 1; i >= 0; i--)
                depthBuffer[i] = float.MaxValue;
        }

        public void Render(Camera camera, params Mesh[] meshes)
        {
            var viewMatrix = Matrix.LookAtLH(camera.Position, camera.Target, Vector3.UnitY);
            var projectMatrix = Matrix.PerspectiveFovRH(0.78f,
                (float)renderWidth / renderHeight,
                0.01f, 1f);
            foreach (var mesh in meshes)
            {
                var worldMatrix = Matrix.RotationYawPitchRoll(mesh.Rotation.Y, mesh.Rotation.X, mesh.Rotation.Z) *
                                  Matrix.Translation(mesh.Position);
                var transformMatrix = worldMatrix * viewMatrix * projectMatrix;

                for (var i = 0; i < mesh.Faces.Length; i++)
                {
                    var face = mesh.Faces[i];
                    var v1 = mesh.Vertices[face.A];
                    var v2 = mesh.Vertices[face.B];
                    var v3 = mesh.Vertices[face.C];

                    var p1 = Project(v1, transformMatrix);
                    var p2 = Project(v2, transformMatrix);
                    var p3 = Project(v3, transformMatrix);

                    var color = 0.25f + i % mesh.Faces.Length * 0.75f / mesh.Faces.Length;
                    DrawTriangle(p1, p2, p3, new Color4(color, color, color, 1));
                }
            }
        }

        public void Present()
        {
            bmp.WritePixels(new Int32Rect(0, 0, renderWidth, renderHeight),
                backBuffer, bmp.BackBufferStride, 0);
        }

        private void DrawTriangle(Vector3 p1, Vector3 p2, Vector3 p3, Color4 color)
        {
            if (p1.Y > p2.Y)
            {
                var tmp = p1;
                p1 = p2;
                p2 = tmp;
            }

            if (p2.Y > p3.Y)
            {
                var tmp = p2;
                p2 = p3;
                p3 = tmp;
            }

            if (p1.Y > p2.Y)
            {
                var tmp = p1;
                p1 = p2;
                p2 = tmp;
            }

            float dp1p2, dp1p3;

            if (p1.Y < p2.Y)
                dp1p2 = (p1.X - p2.X) / (p1.Y - p2.Y);
            else
                dp1p2 = 0;

            if (p1.Y < p3.Y)
                dp1p3 = (p1.X - p3.X) / (p1.Y - p3.Y);
            else dp1p3 = 0;

            // First case where triangles are like that:
            // P1
            // -
            // -- 
            // - -
            // -  -
            // -   - P2
            // -  -
            // - -
            // -
            // P3
            if (dp1p2 > dp1p3)
                Parallel.For((int) p1.Y, (int) p3.Y + 1, y =>
                {
                    if (y < p2.Y)
                        ProcessScanLine(y, p1, p3, p1, p2, color);
                    else
                        ProcessScanLine(y, p1, p3, p2, p3, color);
                });
            // Second case where triangles are like that:
            //          p1
            //          - 
            //         --
            //       -  -
            //     -    -
            // p2 -     -
            //      -   -
            //        - -
            //          -
            //          p3
            else
                Parallel.For((int) p1.Y, (int) p3.Y + 1, y =>
                {
                    if (y < p2.Y)
                        ProcessScanLine(y, p1, p2, p1, p3, color);
                    else
                        ProcessScanLine(y, p2, p3, p1, p3, color);
                });
        }

        private void ProcessScanLine(int y, Vector3 pa, Vector3 pb, Vector3 pc, Vector3 pd, Color4 color)
        {
            var grandientA = !Equals(pa.Y, pb.Y) ? (y - pa.Y) / (pb.Y - pa.Y) : 1;
            var grandientB = !Equals(pc.Y, pd.Y) ? (y - pc.Y) / (pd.Y - pc.Y) : 1;

            var xs = (int) Interpolate(pa.X, pb.X, grandientA);
            var xe = (int) Interpolate(pc.X, pd.X, grandientB);

            var zs = Interpolate(pa.Z, pb.Z, grandientA);
            var ze = Interpolate(pc.Z, pd.Z, grandientB);

            for (var x = xs; x < xe; x++)
            {
                var grandient = (x - xs) / (xe - xs);
                var z = Interpolate(zs, ze, grandient);
                DrawPoint(new Vector3(x, y, z), color);
            }
        }

        private void DrawPoint(Vector3 point, Color4 color)
        {
            if (point.X >= 0 && point.X < renderWidth &&
                point.Y >= 0 && point.Y < renderHeight)
                PutPixel((int)point.X, (int)point.Y, point.Z, color);
        }

        private void PutPixel(int x, int y, float z, Color4 color)
        {
            var index = x + y * renderWidth;

            lock (lockBuffer[index])
            {
                if (depthBuffer[index] < z)
                    return;
                depthBuffer[index] = z;

                var i = index * 4;
                backBuffer[i] = (byte) (color.Blue * 255);
                backBuffer[i + 1] = (byte) (color.Green * 255);
                backBuffer[i + 2] = (byte) (color.Red * 255);
                backBuffer[i + 3] = (byte) (color.Alpha * 255);
            }
        }

        private Vector3 Project(Vector3 coord, Matrix transMat)
        {
            var point = Vector3.TransformCoordinate(coord, transMat);
            var x = point.X * renderWidth + renderWidth / 2f;
            var y = point.Y * renderHeight + renderHeight / 2f;
            return new Vector3(x, y, point.Z);
        }

        public async Task<Mesh[]> LoadJsonFileAsync(string fileName)
        {
            var meshes = new List<Mesh>();
            var file = File.OpenText(fileName);
            var data = await file.ReadToEndAsync();
            dynamic jsonObject = JsonConvert.DeserializeObject(data);

            for (var meshIndex = 0; meshIndex < jsonObject.meshes.Count; meshIndex++)
            {
                var verticesArray = jsonObject.meshes[meshIndex].vertices;
                // Faces
                var indicesArray = jsonObject.meshes[meshIndex].indices;

                var uvCount = jsonObject.meshes[meshIndex].uvCount.Value;
                var verticesStep = 1;

                // Depending of the number of texture's coordinates per vertex
                // we're jumping in the vertices array  by 6, 8 & 10 windows frame
                switch ((int) uvCount)
                {
                    case 0:
                        verticesStep = 6;
                        break;
                    case 1:
                        verticesStep = 8;
                        break;
                    case 2:
                        verticesStep = 10;
                        break;
                }

                // the number of interesting vertices information for us
                var verticesCount = verticesArray.Count / verticesStep;
                // number of faces is logically the size of the array divided by 3 (A, B, C)
                var facesCount = indicesArray.Count / 3;
                var mesh = new Mesh(jsonObject.meshes[meshIndex].name.Value, verticesCount, facesCount);

                // Filling the Vertices array of our mesh first
                for (var index = 0; index < verticesCount; index++)
                {
                    var x = (float) verticesArray[index * verticesStep].Value;
                    var y = (float) verticesArray[index * verticesStep + 1].Value;
                    var z = (float) verticesArray[index * verticesStep + 2].Value;
                    mesh.Vertices[index] = new Vector3(x, y, z);
                }

                // Then filling the Faces array
                for (var index = 0; index < facesCount; index++)
                {
                    var a = (int) indicesArray[index * 3].Value;
                    var b = (int) indicesArray[index * 3 + 1].Value;
                    var c = (int) indicesArray[index * 3 + 2].Value;
                    mesh.Faces[index] = new Face {A = a, B = b, C = c};
                }

                // Getting the position you've set in Blender
                var position = jsonObject.meshes[meshIndex].position;
                mesh.Position = new Vector3((float) position[0].Value, (float) position[1].Value,
                    (float) position[2].Value);
                meshes.Add(mesh);
            }

            return meshes.ToArray();
        }

        private float Clamp(float value, float min = 0, float max = 1)
        {
            return Math.Max(min, Math.Min(value, max));
        }

        private float Interpolate(float a, float b, float gradient)
        {
            return a + (b - a) * Clamp(gradient);
        }
    }
}