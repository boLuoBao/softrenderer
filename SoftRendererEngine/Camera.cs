﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace SoftRendererEngine
{
    class Camera
    {
        public Vector3 Position;
        public Vector3 Target;
    }
}
