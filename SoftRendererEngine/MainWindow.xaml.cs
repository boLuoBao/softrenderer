﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SharpDX;

namespace SoftRendererEngine
{
    /// <summary>
    ///     MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Camera camera = new Camera();
        private readonly Device device;
        private Mesh[] meshes;
        private DateTime previourTime;

        public MainWindow()
        {
            InitializeComponent();

            var bmp = new WriteableBitmap(640, 480, 72, 72, PixelFormats.Bgra32, null);
            device = new Device(bmp);
            frontBuffer.Source = bmp;

            LoadMeshesAsync();

            camera.Position = new Vector3(0, 0, 10f);
            camera.Target = Vector3.Zero;

            previourTime = DateTime.Now;
        }

        
        private void CompositionTarget_Rendering(object sender, object e)
        {
            device.Clear(0, 0, 0, 255);
            foreach (var mesh in meshes)
                mesh.Rotation = new Vector3(mesh.Rotation.X, mesh.Rotation.Y + 0.01f, mesh.Rotation.Z);
            device.Render(camera, meshes);
            device.Present();

            var fpsValue = (DateTime.Now - previourTime).Milliseconds;

            previourTime = DateTime.Now;
            fps.Text = (1000 / fpsValue).ToString();
        }

        private async void LoadMeshesAsync()
        {
            meshes = await device.LoadJsonFileAsync("monkey.babylon");
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }
    }
}